
%\begin{titlepage}  

\begin{sloppy}
	
\twocolumn
\begin{center}
	\noindent\textbf{The response later for SIGMOD submission ID:290}
\end{center}

\noindent\textbf{Response to Meta-Reviewer.}

We would like to thank the meta-reviewer and all reviewers for providing insightful comments to help us improve this manuscript. 
We have clarified more details about the influence formulation $I(S)$ in Section~\ref{Sec:problem_formulation}, which is raised by Reviewer 2 and Reviewer 4.
Following the suggestion of Reviewer 2, we have added the complexity analysis of ALS and that of BLS in Section~\ref{ls1} and Section~\ref{ls2}, respectively.
We have rewritten the theoretical analysis in Section~\ref{proof} to make it clearer.
We have provided more details on the experiment, such as including a more detailed evaluation between our methods and the baseline, adding the number of advertisers for each experiment setup, and adding the time cost unit.
We have responded to Reviewer 4 regarding the hardness of our problem, the intuitive meaning of regret minimization, and how our problem is different from the alternative problem proposed by Reviewer 4.
All major revisions have been highlighted in \tocheck{blue color}.


\revSpace

\noindent\textbf{Reviewer 2.}

\textbf{O1. In the problem definition, it will be better to explain more about the function $I(S)$ and why defines a trajectory database in the MROAM problem.}

A1. 
Thanks for the suggestion. The explanation on $I(\billboards)$ was in Section 7.1.2 (i.e.  the experiment part) of the original submission. In this revision, we have followed the suggestion to explain more details on the influence of a billboard $I(\billboard)$ and the influence of a billboard set $I(\billboards)$, and move them into  Section 3.1 as preliminaries for the problem formulation.
%\bao{add descriptions on how we calculate each of them in a bottom up way here, rather than just asking the reviewer to refer to section 3.}

Notably, we follow the same settings of the existing work on Out-of-Home advertising~\cite{DBLP:conf/kdd/ZhangBLLZP18,TKDD_OOH}, where $I(\billboards)$ is the number of moving users impressed by $S$.
We define a trajectory database in the MORAM problem because, for concreteness, we study our problem under the Out-of-Home advertising scenario. The trajectory records the real user movement, which allows us to accurately check whether a user can be impressed by a billboard.

A billboard database is denoted as $\bd=\{\billboard_1, \cdots, \billboard_{|\bd|}\}$, where each billboard is positioned at one location. 
%where each billboard has a coordinate. 
A trajectory database is denoted as $\td=\{\traj_1, \cdots, \traj_{|\td|}\}$. Each trajectory is formed by a sequence of locations $\traj_i = \{p_1, ..., p_{|\traj_i|}\}$, where each location $p_j$ is a pair of latitude and longitude. 
A binary function $pr(\billboard,\traj)$ indicates whether a billboard $\billboard$ could influence a user who moves along the trajectory $\traj$.
If there exists $p_j \in \traj$ such that the distance between $p_j$ and $\billboard$ is not greater than a given threshold, $pr(\billboard,\traj) = 1$; otherwise, $pr(\billboard,\traj)=0$. Subsequently, $pr(\billboards,\traj_j)=1 - \prod_{\billboard_i \in \billboards} (1- pr(\billboard_i,\traj_j))$ models the influence of a set of billboards $\billboards$ to $\traj_j$, where $(1- pr(\billboard_i,\traj_j))$ is the probability that $\billboard_i$ cannot influence $\traj_j$. Notation $\I({\billboards})$ is used to represent the influence of a set of billboards, where $\I({\billboards})=\sum_{\traj_j \in \td} pr(\billboards,\traj_j)$.

 

\ansSpace

\textbf{O2. In Theorem 4.1, the authors should clarify what function $I(S)$ is used in the reduction (I guess it is the sum of influence value of each billboard in set $S$). Moreover, the authors should also explain how to set the payment $L_i$, although $L_i$ perhaps does not matter.}

A2. For the formation of $I(S)$, we have included more details about the calculation of $I(S)$ in Section 3. Please refer to our response A1 to your previous comment O1 (of Reviewer 2) for more details. For the proof of Theorem 4.1, it does not matter how to formulate $I(S)$ as it just indicates the influence of a billboard set. 
Also, we would like to highlight that, for the proof of Theorem 4.1, it does not matter how to set the payment $L_i$. This is because if we can find a solution for the N3DM problem, then the regret will be zero for any payment $L_i$. Last, please refer to the paragraph starting with ``Advertiser's Payment" in Section~\ref{sec:keypara} to find out more about the setup of $L_i$ used in our experiment. 

\ansSpace

\textbf{O3. In Section 6, please provide the time complexity of the proposed algorithms: ALS and BLS.}

A3. Following the suggestion, we have added the time complexity analysis of ALS and that of BLS in Section~\ref{ls1} and Section~\ref{ls2}, respectively.
Both ALS and BLS try to find a smaller regret based on a solution generated from Algorithm 3. The core difference lies in how they execute the local search, as stated in Line 3.9 of Algorithm 3. In the following, we present the time complexity analysis of ALS and BLS, respectively.

\textbf{\underline{ALS}}: For each while loop, ALS takes $O(|A|^2)$ time to exchange billboards between advertisers. The while loop will repeat $t$ times. Therefore, the time complexity of ALS is $O(t|A|^2)$. Theoretically, $t$ can be as large as the factorial of $|A|$. However, in practice it is usually much smaller. For example, under the default setting of $\infPer=100\%$ and $\avgInfP=5\%$ in experiments, we find that $t$ is always smaller than 5.

\textbf{\underline{BLS}}: For each while loop, there are three steps: (1) switch two billboards between two advertisers; (2) switch a billboard with an unused billboard; and (3) release a billboard from an advertiser. Their time complexity are $O(|T||A|^2)$, $O(|T||A|)$, and $O(|T||A|)$, respectively. Subsequently, the time complexity of one loop is $O(|T||A|^2)$. Consequently, the time complexity of BLS is  $O(m|T||A|^2)$, where $m$ is the number of times the loop repeats. Under the default setting of $\infPer=100\%$ and $\avgInfP=5\%$, we find that $m$ is always smaller than 10 in our experiments.

It is worth noting that the theoretical time complexity of the local search is normally large because it may search for all combinations in the worst-case scenario.  In practice, depending on the search strategy, the cost of a local search could be very different from the worst-case time complexity. 
%Therefore, the analysis of the time complexity of the local search is not indispensable.

\ansSpace

\textbf{O4. In Section 6.3, please explain why/how the rewired formulation mimics the original objective in more details. Maybe, it is better to provide a toy example.}

A4. We rewrite the proof to make it clearer. As explained in the paragraph just below Equation (2),  $\regret'$ mimics $\regret$ in the way that $R'(S_i)=L_i-R(S_i)$. Moreover, $\regret'$ is neither monotone nor submodular. The only reason we transform them into a maximization version is to facilitate our proof of Theorem 1 on the approximation ratio. Consequently, maximizing $R’$ is essentially a dual problem of minimizing the regret $\regret$. 

\ansSpace

\textbf{O5. In Table 6, it may be better to add the number of billboards and advertisers used in the experiments.}

A5. For the number of billboards used in the experiments, it is presented in Table 5: there are 1462 billboards in NYC and 4092 billboards in SG. 

For the number of advertisers, we have added the exact number of advertisers for each experiment at the caption of corresponding figures. For example, in Figure~\ref{fig:regret_alpha_inf1_sg}, the number of advertisers is $|\advers|=100$ when $p(\avgInf) = 1\%$.   
%\bao{I did not see those numbers highlighted in the revision.} 

As explained at Section 1 (the paragraph starting with ``Empirical Evaluation") and the beginning of Section 7, one novel aspect of this work is that our experiment setup exhibits various \textit{real-world demand-supply relationships}. The number of advertisers depends on two important parameters: (1) the Demand-Supply ratio $\infPer$,  which reflects whether the global demand of a group of advertisers is (far) below, close to, or over the maximum supply of the host at macro-level; (2) the Individual Demand Ratio $\avgInfP$, which represents the advertiser whose individual demand is high, medium, or low at micro-level. 

Moreover, as we explained in Section~\ref{Sec:experiment}, a straightforward setup that selects different numbers of advertisers $|\advers|$, and then randomly sets a demand $\requiredI_i$ for each advertiser $\adver_i \in \advers$ from an estimated range cannot well reflect different scenarios in reality. This is the reason that we propose two key questions at the beginning of Section 7. Accordingly, we study and answer these two questions by introducing the Demand-Supply ratio $\infPer$ and the Individual Demand Ratio $\avgInfP$.

%\bao{add sth like "Also, we have explained why simply varying the number of advertisers and xxx may not well reflect different scenarios in reality, at the xxx paragraph of Section xxx."}
Consequently, the number of advertisers can be easily calculated based on $\infPer$ and $\avgInfP$. For example, under the default setting of $\infPer=100\%$ and $\avgInfP=5\%$, we have $100\%/5\%=20$ advertisers. 
We follow the suggestion to explicitly show the number of advertisers in our revision.

\ansSpace

\textbf{O6. The efficiency of BLS and ALS. As shown by the experiments, the cost can easily reach over $10^2$ seconds. Not sure if this is really tolerable in the real applications.}

A6. The experiment is conducted over more than four thousand billboards and more than two million trajectories, and our MROAM is a combinatorial optimization problem where there is no real-time response need. For example, in the default setting where there are four thousand billboards and twenty advertisers, the possible combinations are $20^{4000}$.  
%\bao{Would it be better to show the search space is xx combinations at most, to give the reader a more straightforward impression? R2 should not be a tech person from most of her/his reviews.} 
Considering the above, we believe the time cost of $10^2$ (unit: second) is reasonable. In addition, it is worth highlighting that the \emph{effectiveness} of our solutions (i.e., getting a regret as small as possible) is our primary goal.  

\ansSpace

\textbf{O7. There are some issues in the experiments:
(1) It seems that BLS often has a better effectiveness than ALS (with a higher time complexity). If it is true, why ALS is also necessary? Please add more explanations.
(2) In some figures (eg Figure 5d, 6d), the baseline G-global has a similar effectiveness with the proposed method ALS. Please explain the reason in details.
(3) Besides, please provide the unit of the time cost in the experimental figures.}

A7. 
(1) ALS and BLS are designed to serve different needs on the efficiency-effectiveness trade-off: if the host wants to get a solution in a short time and is willing to sacrifice the effectiveness, ALS is preferred; otherwise, BLS is preferred. Moreover, we can study the difference between a coarse-grained local search strategy and a fine-grained local search strategy.
(2) Figures 5(d) and 6(d) reflect our Case 4, i.e., the global demand is high $\infPer=100\%$  (total demand = total supply) and meanwhile each advertiser has a high individual demand $\avgInfP$. As we mentioned is Section~\ref{exp:NYC}, Case 4, it is very likely that the host cannot satisfy all advertisers, and every unsatisfied advertiser will lead to a high regret. For example, when $\avgInfP = 20\%$ and $\infPer=100\%$, there are only five advertisers. Thus, the advantage of ALS will not be significant as compared with the baseline G-global. 
(3) The unit is second. We have included the time unit in our description and all the figures related to efficiency study (i.e., Figure~\ref{fig:time_alpha_sg} and Figure~\ref{fig:time_alpha_nyc}).

\textbf{O8. There are some typos. For instance, on Page 4, ``Set beta = 0'' may be ``gamma = 0''.}

A8. Thanks for pointing them out. We have carefully proofread the paper and fixed all the typos that we could find.

\revSpace

\noindent\textbf{Reviewer 4.}

\textbf{O1. The studied problem appears artificial in more than one way. (1) It is not clear how realistic it is to assign an integer influence to each billboard, or the advertiser seeking to reach an integer influence score.}
	
\textbf{(2) It is also not clear if the billboards are independent from each other in influence, or if the same billboard can have different influence on different advertiser. When the billboards have integer influence, these aspects become murky and the work does not make any attempt to address any of these questions.}
	
\textbf{(3) Perhaps, a simpler and cleaner problem needs to be studied first:
each bill board has a 0/1 influence on an advertiser and is associated with a cost. the same billboard may influence more than one advertiser. Each advertiser wants to reach an influence target (have at least x billboards that positively influence them) by paying a budgeted cost. The goal would be allocate the resources by the influence provider such that her monetary profit is maximized while satisfying as many demands as possible (or even could be studied as a regret minimization problem considering the two aforementioned regrets).
I believe this is a cleaner problem, still NP-hard, but the optimization problem could be studied as a submodular maximization problem under matroid constraints and local search will guarantee approximation factor.} %\bao{I suggest we segment his comments to several major parts, and show our response to each individually.}

A1.  
(1) We follow the suggestion to explain the influence of billboard (i.e., $I(\billboard)$) and that of billboard set $I(\billboards)$ in Section 3 for a more smooth understanding of the problem settings.
Further, we note that how to set the influence function and our formulation of regret minimization are orthogonal.

The influence score is not necessarily an integer; we guess one possible reason that the reviewer had this impression is that, in our motivating example (Example 1 in Section 1), we use integer influence score to present the intuitive idea of our problem.

The choice of influence function is orthogonal to the regret minimization problem studied in this paper, and our solution can work with any choice of influence function. Consequently, in the original submission, we chose to present details on the influence function used in our experiment setup (please see Sec 7.1.2), where we did not propose any new function but just followed the setup used by existing works \cite{DBLP:conf/kdd/ZhangBLLZP18,TKDD_OOH}. Please refer to Section 3.1 of this manuscript for details on how the influence of a billboard set, $I(S)$, is calculated (in existing works \cite{DBLP:conf/kdd/ZhangBLLZP18,TKDD_OOH}).


The regret minimization proposed in this paper is not an artificial problem, but motivated by a real-world setting adopted by multiple advertising companies. We will show more details in our response to next question.

A1.
(2) Please note that (i) the influence of a billboard is not restricted to be an integer, while we use integer influence in Example 1 mainly to facilitate the illustration of the idea; (ii) the billboards are independent from each other in terms of influence, and the influence of a billboard is independent on the advertiser; 
%will not have different influence for a different advertiser; 
and (iii) the influence is to the real users whose movements are captured by the trajectories but not to the advertiser.

A1.
(3) We want to respond on the simpler problem suggested by the reviewer: setting a binary value for the influence of a billboard to a user trajectory. Under this simpler setting, the objective function is NOT submodular, as shown in the example below.

\textit{
Example.~
Given a user trajectory set $\{t_1, \cdots, t_6\}$ and two billboard sets $S_1$ and $S_2$, $S_1$ influences five trajectories $\{ t_1, \cdots, t_5 \}$, and $S_2$ influences four trajectories $\{ t_1, \cdots, t_4 \}$. 
When the influence of a billboard to a user is 0/1, the influence of $S_1$ is $I(S_1)=5$ and that of $S_2$ is $I(S_2)=4$. 
Given another billboard $o_1$, let’s assume $o_1$ influences $t_6$, and let $S'_1=S_1 \cup \{o_1\}$, and $S'_2=S_2 \cup \{o_1\}$. 
Accordingly, we have $I(S'_1)=6$ and $I(S'_2)=5$. 
Considering an advertiser with $I=6$ and $L=6$, we have $R(S_1)=6-5r$, $R(S_2)=6-4r$, $R(S'_1)=0$, and $R(S'_2)=6-5r$, where $r \in [0,1]$ is the penalty ratio. 
Hence, $R(S_1)-R(S'_1)> R(S_2)-R(S'_2)$. Consequently, the objective function $R(S)$ is neither monotone nor submodular.
}

This also explains why the setup of influence function does not change the hardness of the problem studied.

\ansSpace

\textbf{O2. It is not obvious why a complicated regret model (equation 1) needs to be designed. Especially, the form of excessive influence regret requires further justification.}

A2.
As justified in Section 1, it is a standard practice for advertising companies (including LARMA~\cite{lamar} in USA, JCDecaux~\cite{jcdecaux} in Singapore and Juping~\cite{juping} in China) to submit a campaign proposal to the host by specifying a demanded influence and a corresponding committed payment that will only be fully paid if the demand is achieved. We build our regret model based on these real scenarios.
We have justified why we need the following two types of regret (i.e., type 1 revenue regret and type 2 excessive influence regret) in Section 3.2 and Example 1.
Firstly, as the host can receive the full payment only when the influence demanded by the advertiser is achieved, the regret model needs to capture the lost revenue from the unsatisfied advertiser (i.e., revenue regret). 
Secondly, excessive influence regret is necessary because the excessive influence is a ‘free service’ provided by the host that unfortunately cannot be captured by the (earned) revenue.
In addition, it is worth highlighting that considering both types of regret is widely adopted in many advertising problems, such as [2] in the context of viral marketing on social media in Section 2.1.

\ansSpace

\textbf{O3. The authors eventually transform the minimum regret problem to a maximum revenue problem and then show how local search will have approximation guarantee. This is a big leap without much of justification. It may be worthwhile to study the problem as a revenue maximization problem from the beginning.}

A3.
Minimizing the lost revenue part of a regret is dual to minimizing the gained revenue; however, the ``excessive influence'' regret cannot be covered by ``revenue maximization''. The only reason that we transform the minimum regret problem into a maximization version is to facilitate our proof of Theorem 1 on the approximation ratio in a more straightforward manner. However, please note that the dual problem here is NOT simply a ``revenue maximization'' problem. For example, when the host has an excessive number of available billboards, maximizing the gained revenue is trivial as all advertisers' demands can be fulfilled by deploying all the billboards. However, this leads to unnecessary billboard deployment that can otherwise be used to meet demands which have yet to come. 


\end{sloppy}





%\end{titlepage}