%\vspace{-.5em}
\section{PROBLEM HARDNESS}
\label{Sec:hardness}

In this section, we conduct a theoretical analysis on the hardness of the \problem problem.
%First, we show that \problem is NP-hard.
%Then, we show that \problem is NP-hard to approximate within any factor.

\begin{thm} \label{NP-hard}
	\problem is NP-hard, and is NP-hard to approximate within any constant factor.
\end{thm}


We use a reduction from the numerical 3-dimensional matching (N3DM) problem \cite{DBLP:books/fm/GareyJ79} to prove the hardness of \problem.
Let $b$ denote a bound and $X$, $Y$ and $Z$ denote three multisets of integers respectively, each containing $n$ elements. 
The N3DM is a decision problem that asks whether there is a matching relation $M$ of $X \times Y \times Z$ such that every integer in $X$, $Y$ and $Z$ occurs exactly once, and for every triple $(x_i,y_i,z_i)\in M$, we have $x_i+y_i+z_i=b$ hold. This problem is known to be NP-complete. It is noted that the matching exists only if $b =(\sum X+ \sum Y + \sum Z)/n$.


We reduce the N3DM decision problem to \problem with the following process:

\begin{enumerate}  \setlength\itemsep{-.5mm}
	\item Set the number of billboards in $\bd$ to be $3n$. Set the number of advertisers in $\advers$ to be $n$. Set $\gamma = 0$.
	%$\theta = 100\%$ and $\beta = 0$. 
	\item We divide billboards into three disjoint sets $D_1$, $D_2$ and $D_3$ equally, and map each element in $X$, $Y$ and $Z$ in the N3DM problem to a billboard of the three sets. Then, $|X|=|Y|=|Z|=n$, $|D_1|=|D_2|=|D_3|=n$, and $b =(\sum X+ \sum Y + \sum Z)/n$. 
	\item For each billboard $\billboard_i \in \bd$, let $\billboard_i$ influence a disjoint set of trajectories. 
	The influence of $\billboard_i$ is set as the integer value of the corresponding element in the N3DM.
	To facilitate proofs, we use $\billboard_i$ to denote its influence in this section only. 
	\item Let $c$ be a large number. We revise the influence value of all billboards as
	$\forall \billboard_i \in D_1, \billboard_i \leftarrow c + \billboard_i$, $\forall \billboard_j \in D_2, \billboard_j \leftarrow 3 \cdot c + \billboard_j$, and $\forall \billboard_k \in D_3, \billboard_k \leftarrow 9 \cdot c + \billboard_k$.
	After the revision, we set the demanded influence of all advertisers to be 
	$\requiredI_i=b + 13\cdot c$.
	Note that, when $c\rightarrow \infty$, the minimum regret value $0$ is achieved for the above setting only if for all $\billboards_i \in \billboards$, $\billboards_i = \{(\billboard_i,\billboard_j,\billboard_k)|\billboard_i \in D_1, \billboard_j \in D_2, \billboard_k \in D_3\}$.
\end{enumerate}
Clearly, the reduction can be done in polynomial time.
Next, we are ready to prove the hardness of \problem. 


\begin{proof} 
	We show that the answer to the $\problem$ decision problem is YES (if the minimum regret value is zero) \textit{if} and \textit{only if} the answer to the N3DM decision problem is YES.
	
	\vspace{.3em}
	\noindent\textbf{The If Direction.}
	When the answer to the $\problem$ decision problem is YES, there must exist $\billboards$ such that $\regret(\billboards)=0$.
	This implies that for every billboard set $\billboards_i \in \billboards$, $ \regret(\billboards_i) = 0$, i.e., $\mathcal{I}_i = I(\billboards_i)$.
	It is because (1) $I(D_1 \cup D_2 \cup D_3) = \sum_{\billboards_i \in \billboards} I(\billboards_i)$, and (2) for $ 1 \leq i < j \leq n$, $\mathcal{I}_i = \mathcal{I}_j$.
	If for any billboard set $\billboards_i \in \billboards$, $I(\billboards_i) > \mathcal{I}_i$, then there must exist at least one billboard set $\billboards_j$ where $I(\billboards_j) < \mathcal{I}_j$, which implies $\regret(\billboards_j)>0$.
	Since each element $x_i \in X$, $y_i \in Y$ and $z_i \in Z$ is mapped to the influence value of the corresponding billboard, for each triple $(x_i,y_i,z_i) \in M$, $(x_i+y_i+z_i) = I(\billboards_i)-13 \cdot c  = \mathcal{I}_i-13 \cdot c = b$.
	%Otherwise, for any triple $ (x_i,y_i,z_i) \in M$, such that $(x_i+y_i+z_i) > b$, then there must be at least one triple that $(x'_j+y'_j+z'_j) < b$.
	%Consequently, $\mathcal{I}_j > I(\billboards_j)$,  $\regret_j(\billboards_j)>0$, and $\regret(\billboards) > 0$.
	As a result, the answer to the N3DM decision problem is YES.
	
	\vspace{.3em}
	\noindent\textbf{The Only-if Direction.}
	When the answer to the N3DM decision problem is YES, there must exist $M \subset X \times Y \times Z$ such that, for all $(x_i, y_i, z_i) \in M$, $(x_i + y_i + z_i) = b$.
	Since the value of each $x_i$, $y_i$, $z_i$ is mapped to the influence value of the corresponding billboard, for the corresponding billboard set $\billboards_i \in \billboards$, $I(\billboards_i) = b + 13\cdot c = \mathcal{I}_i$.
	We have $\regret(\billboards) = 0$ as $\regret(\billboards_i) = 0$ for all $\billboards_i \in \billboards$.
	Hence the answer to the decision problem of $\problem$ is YES.
	
	Based on the above arguments, the N3DM decision problem is equivalent to deciding whether there is a billboard deployment strategy to achieve zero regret. Since the N3DM decision problem is NP-complete, the decision problem of \problem is NP-complete, and the optimization problem is NP-hard, even if $|\td|$ is restricted to be the number of polynomial value $|\bd|$.
	
	\vspace{.3em}
	\noindent\textbf{Approximation Hardness.} We next show: if \problem can be approximated with any factor in polynomial time, then the N3DM decision problem can be solved in polynomial time. Let $OPT_n$ denote the number of triples whose summations are not $b$ in N3DM. Let $OPT_m$ denote the instance of \problem to which the N3DM problem matching is reduced. We can conclude $OPT_n = 0$ if and only if $OPT_m = 0$.
	%(2) Let $m_1, ..., m_n$ be the triples of $M$, such that the sum of each triple is $b$.
	%We split the corresponding $\billboard$ into $\billboards_1, ..., \billboards_n$, so that for each $\billboards_i \in \billboards$, $I(\billboards_i) = b$, while $|\billboards_i|=3$.
	Suppose $ALG$ is an algorithm that approximates \problem within a factor of $\tau$.
	Then, the minimum $\regret(\billboards)$ achieved by $ALG$ on any instance of \problem is smaller than $\tau \cdot OPT_m$, i.e.,  $\regret(\billboards) \leq \tau \cdot OPT_m$. %$\regret_{ALG}(\billboards) \leq \tau \cdot OPT_m$. 
	Hence, when $OPT_n = OPT_m = 0$, we have $\regret(\billboards) = 0$; when  $OPT_n \neq 0$, we have $\regret(\billboards) \geq OPT_m > 0$. 
	Based on the above, we can solve N3DM within polynomial time by checking whether  $\regret(\billboards) = 0$, which is impossible unless NP=P. Hence, it is NP-hard to approximate \problem within any constant factor. 
	%On the above instance, the minimum regret achieved by $ALG$ is $\regret_{ALG} \leq \tau \cdot 0 = 0$.
	%Thus, if one can approximate \problem in polynomial time, then the N3DM decision can be approximated in polynomial time, which is shown above to be impossible unless $P = NP$.
\end{proof}

	

