#set xlabel "{/Symbol q}" font ",20" offset 0,-0.3,0
#set xtics (10, 20, 40, 80)
set xtics font ",20" offset 0,-0.3,0

# Don\t show the legend in the chart
set nokey
#set key font ",15" inside vertical top right 

# Thinner, filled bars
set boxwidth 0.3
set style fill solid 1.00 
set border 3
set xtics nomirror
set ytics nomirror

#set size 0.6

# Lighter grid lines
set grid ytics lc rgb "#ffffff"

# Manual set the Y-axis range
set yrange [0 to 1100000]
# Show human-readable Y-axis. E.g. "100 k" instead of 100000.
set ytics font ",20"
set format y '%.0s %c'

plot "regret-theta-100.dat" using 2:xticlabels(1) with boxes lt rgb "#000000" fs pattern 1 ,\
     "regret-theta-100.dat" using 3 with boxes lt rgb "#000000" fs pattern 2,\
     "regret-theta-100.dat" using 4 with boxes lt rgb "#000000" fs pattern 3,\
	 "regret-theta-100.dat" using 0:2:5 with labels font ",20" offset 3,-3 rotate by 270 not,

set terminal postscript eps color
set terminal postscript eps size 100,400
set terminal postscript eps fontscale 1.5
set output "regret-theta-100.eps"
replot

quit		