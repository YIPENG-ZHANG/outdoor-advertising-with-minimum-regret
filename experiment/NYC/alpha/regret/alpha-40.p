set xlabel "Average Demand (*1000)" font ",20" offset 0,-0.3,0
set xtics font ",20" offset 0,-0.3,0


set ylabel "Regret" font ",20" offset 0,0,0
set yr[1000:200000]
set format y "10^{%L}"
set ytics (1000, 10000, 100000)
set ytics font ",20" rotate offset 0,0,0
set logscale y 10


set style data histogram
set style histogram cluster gap 1.5
set style fill solid border
set boxwidth 0.9

set key font ",15" inside vertical top right 

set size 0.6
	
plot 'alpha-40.dat' using ($2):xticlabels(1) title "GreedyForOne" fs solid 1 lc rgb "black" fillstyle  pattern 2,\
'alpha-40.dat' using ($3):xticlabels(1) title "GreedyForAll" fs solid 1 lc rgb "green" fillstyle  pattern 1,\
'alpha-40.dat' using ($4):xticlabels(1) title "RLS-Adv" fs solid 1 lc rgb "blue" fillstyle  pattern 5,\
'alpha-40.dat' using ($5):xticlabels(1) title "RLS-Bil" fs solid 1 lc rgb "red" fillstyle  pattern 3,\

set terminal postscript eps color
set terminal postscript eps size 5,4
set terminal postscript eps fontscale 1
set output "alpha-40.eps"
replot

quit		